module.exports = function(grunt) {

  grunt.initConfig({
    sass: {                              // Task 
      dist: {                            // Target 
        options: {                       // Target options 
          style: 'expanded'
        },
        files: {                         // Dictionary of files 
          'public/css/style.css': 'public/css/style.scss',
          'public/css/promo.css': 'public/css/promo.scss'
        }
      }
    },
    uglify: {
      build: {
        files: {
          'public/js/main.min.js': 'public/js/main.js',
          'public/js/promo.min.js': 'public/js/promo.js'
        }
      }
    },
    cssmin: {
      build: {
        files: {
          'public/css/style.min.css': 'public/css/style.css',
          'public/css/promo.min.css': 'public/css/promo.css'
        }
      }
    },
    imagemin: {
      dynamic: {                         // Another target 
        options: {                       // Target options 
          optimizationLevel: 7,
        },
        files: [{
          expand: true,                  // Enable dynamic expansion 
          cwd: 'public/images/',                   // Src matches are relative to this path 
          src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match 
          dest: 'public/images/dist/'                  // Destination path prefix 
        }]
      }
    },
    watch: {
      scripts: {
        files: ['public/css//*.scss','public/css//*.css','public/js//*.js'],
        tasks: ['sass', 'uglify', 'cssmin']
      },
    }
  });
   
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.registerTask('default', ['sass','uglify','cssmin','imagemin']);

};