// http://spin.js.org/#v2.3.2
!function(a,b){"object"==typeof module&&module.exports?module.exports=b():"function"==typeof define&&define.amd?define(b):a.Spinner=b()}(this,function(){"use strict";function a(a,b){var c,d=document.createElement(a||"div");for(c in b)d[c]=b[c];return d}function b(a){for(var b=1,c=arguments.length;c>b;b++)a.appendChild(arguments[b]);return a}function c(a,b,c,d){var e=["opacity",b,~~(100*a),c,d].join("-"),f=.01+c/d*100,g=Math.max(1-(1-a)/b*(100-f),a),h=j.substring(0,j.indexOf("Animation")).toLowerCase(),i=h&&"-"+h+"-"||"";return m[e]||(k.insertRule("@"+i+"keyframes "+e+"{0%{opacity:"+g+"}"+f+"%{opacity:"+a+"}"+(f+.01)+"%{opacity:1}"+(f+b)%100+"%{opacity:"+a+"}100%{opacity:"+g+"}}",k.cssRules.length),m[e]=1),e}function d(a,b){var c,d,e=a.style;if(b=b.charAt(0).toUpperCase()+b.slice(1),void 0!==e[b])return b;for(d=0;d<l.length;d++)if(c=l[d]+b,void 0!==e[c])return c}function e(a,b){for(var c in b)a.style[d(a,c)||c]=b[c];return a}function f(a){for(var b=1;b<arguments.length;b++){var c=arguments[b];for(var d in c)void 0===a[d]&&(a[d]=c[d])}return a}function g(a,b){return"string"==typeof a?a:a[b%a.length]}function h(a){this.opts=f(a||{},h.defaults,n)}function i(){function c(b,c){return a("<"+b+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',c)}k.addRule(".spin-vml","behavior:url(#default#VML)"),h.prototype.lines=function(a,d){function f(){return e(c("group",{coordsize:k+" "+k,coordorigin:-j+" "+-j}),{width:k,height:k})}function h(a,h,i){b(m,b(e(f(),{rotation:360/d.lines*a+"deg",left:~~h}),b(e(c("roundrect",{arcsize:d.corners}),{width:j,height:d.scale*d.width,left:d.scale*d.radius,top:-d.scale*d.width>>1,filter:i}),c("fill",{color:g(d.color,a),opacity:d.opacity}),c("stroke",{opacity:0}))))}var i,j=d.scale*(d.length+d.width),k=2*d.scale*j,l=-(d.width+d.length)*d.scale*2+"px",m=e(f(),{position:"absolute",top:l,left:l});if(d.shadow)for(i=1;i<=d.lines;i++)h(i,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(i=1;i<=d.lines;i++)h(i);return b(a,m)},h.prototype.opacity=function(a,b,c,d){var e=a.firstChild;d=d.shadow&&d.lines||0,e&&b+d<e.childNodes.length&&(e=e.childNodes[b+d],e=e&&e.firstChild,e=e&&e.firstChild,e&&(e.opacity=c))}}var j,k,l=["webkit","Moz","ms","O"],m={},n={lines:12,length:7,width:5,radius:10,scale:1,corners:1,color:"#000",opacity:.25,rotate:0,direction:1,speed:1,trail:100,fps:20,zIndex:2e9,className:"spinner",top:"50%",left:"50%",shadow:!1,hwaccel:!1,position:"absolute"};if(h.defaults={},f(h.prototype,{spin:function(b){this.stop();var c=this,d=c.opts,f=c.el=a(null,{className:d.className});if(e(f,{position:d.position,width:0,zIndex:d.zIndex,left:d.left,top:d.top}),b&&b.insertBefore(f,b.firstChild||null),f.setAttribute("role","progressbar"),c.lines(f,c.opts),!j){var g,h=0,i=(d.lines-1)*(1-d.direction)/2,k=d.fps,l=k/d.speed,m=(1-d.opacity)/(l*d.trail/100),n=l/d.lines;!function o(){h++;for(var a=0;a<d.lines;a++)g=Math.max(1-(h+(d.lines-a)*n)%l*m,d.opacity),c.opacity(f,a*d.direction+i,g,d);c.timeout=c.el&&setTimeout(o,~~(1e3/k))}()}return c},stop:function(){var a=this.el;return a&&(clearTimeout(this.timeout),a.parentNode&&a.parentNode.removeChild(a),this.el=void 0),this},lines:function(d,f){function h(b,c){return e(a(),{position:"absolute",width:f.scale*(f.length+f.width)+"px",height:f.scale*f.width+"px",background:b,boxShadow:c,transformOrigin:"left",transform:"rotate("+~~(360/f.lines*k+f.rotate)+"deg) translate("+f.scale*f.radius+"px,0)",borderRadius:(f.corners*f.scale*f.width>>1)+"px"})}for(var i,k=0,l=(f.lines-1)*(1-f.direction)/2;k<f.lines;k++)i=e(a(),{position:"absolute",top:1+~(f.scale*f.width/2)+"px",transform:f.hwaccel?"translate3d(0,0,0)":"",opacity:f.opacity,animation:j&&c(f.opacity,f.trail,l+k*f.direction,f.lines)+" "+1/f.speed+"s linear infinite"}),f.shadow&&b(i,e(h("#000","0 0 4px #000"),{top:"2px"})),b(d,b(i,h(g(f.color,k),"0 0 1px rgba(0,0,0,.1)")));return d},opacity:function(a,b,c){b<a.childNodes.length&&(a.childNodes[b].style.opacity=c)}}),"undefined"!=typeof document){k=function(){var c=a("style",{type:"text/css"});return b(document.getElementsByTagName("head")[0],c),c.sheet||c.styleSheet}();var o=e(a("group"),{behavior:"url(#default#VML)"});!d(o,"transform")&&o.adj?i():j=d(o,"animation")}return h});

$(function(){
    
    var opts = {
    lines: 17 // The number of lines to draw
    , length: 28 // The length of each line
    , width: 11 // The line thickness
    , radius: 84 // The radius of the inner circle
    , scale: 0.3 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#0089BD' // #rgb or #rrggbb or array of colors
    , opacity: 0.1 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: true // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
    }
    var target = document.getElementById('spinner')
    var spinner = new Spinner(opts).spin(target);

	// Top menu
	$('.menu li').click(function(){
		var value = $(this).data('val');
		var page = $('.page').data('class');
		if (value == page) {
			$('.page').removeClass('down');
			$('.page').data('class', '');
			$('.subpage').removeClass('active');
			$('.menu li a').removeClass('selected');
			closeMobileMenu();
		} else {
			$('.subpage').removeClass('active');
			$('.subpage.'+value).addClass('active');
			$('.page').addClass('down');
			$('.page').data('class', value);
			$('.menu li a').removeClass('selected');
			$(this).find('a').addClass('selected');
			closeMobileMenu();
		}
	});

	// Button supbage
	$('.subpage .nav .button a').click(function(){
		$('.page').removeClass('down');
		$('.page').data('class', '');
		$('.subpage').removeClass('active');
		$('.menu li a').removeClass('selected');
	});

	$('.subpage .nav .alternate a').click(function(){
		$('.subpage').removeClass('active');
		$('.subpage.'+'faq').addClass('active');
		$('.page').addClass('down');
		$('.page').data('class', 'faq');
		$('.menu li a').removeClass('selected');
		$('.menu li[data-val="faq"] a').addClass('selected');
	});

    // Signup
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    function getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	}
    $('#formSub, #formSubMob').submit(function(e){
        e.preventDefault();
        $('.loader-form').fadeIn(500);
        $.ajax({
            type: "POST",
            url: "/"+page+"/submit",
            data: $(this).serialize(),
            dataType: "json",
            success: function(data) {
            	$('.input-wrapper').removeClass('error');
                if (data == null) {
                	$('.input-wrapper.field-email').find('span').text('Oops! Please use a valid email address');
                	$('.input-wrapper.field-email').addClass('error');
                }
                else {
                	if (data.error) {
                		$('.input-wrapper.field-'+data.error.type).find('span').text(data.error.message);
                		$('.input-wrapper.field-'+data.error.type).addClass('error');
                	} else {
                		console.log(data);
                		setCookie("idpref", data.prefinery_id, 1);
                		setTimeout(function(){
                    		window.location='/'+page;
                		},500);
                	}
                }
                $('.loader-form').fadeOut(1000);
            },
            error: function(){
                alert('Error submit data');
                $('.loader-form').fadeOut(1000);
            }
        });
    });

	function closeMobileMenu(){
		$('.menu-mobile').removeClass('open');
		setTimeout(function(){
			$('.menu-mobile').css('z-index','-1');
		}, 500);
	}

	$('.open-menu').click(function(){
		setTimeout(function(){
			$('.menu-mobile').addClass('open');
		}, 300);
		$('.menu-mobile').css('z-index','8');
	});
	$('.close-menu').click(function(){
		closeMobileMenu();
	});
    
    function checkMobileContainer() {
        var winWidth = $(window).width();
        var winHeight = $(window).height();
        if (winWidth < 1024) {
            $('.step1 .desc-head-mobile').height(winHeight-100);
        }
    }
		function checkMobileBackground() {
        var winWidth = $(window).width();
        var winHeight = $(window).height();
        if (winWidth < 1024) {
          $('.page.home').addClass('mobile');
        } else {
					$('.page.home').removeClass('mobile');
				}
    }
    checkMobileContainer();
		checkMobileBackground();
    $(window).resize(function(){
      checkMobileContainer(); 
			checkMobileBackground();
    });
    
    $('.arrow-bottom a').click(function(e){
       $('.page').animate({
        scrollTop: $('.desc-about').offset().top},
        1000);
       e.preventDefault();
    });
	
		$('#code').keyup(function(){
				$(this).val(promocode);
		});
		$('#telephone').focus(function(){
				var thisval = $(this).val();
				var ini = $(this);
				if (thisval=='+62'){
						setTimeout(function(){
								ini.val('+62');
						},500);
				}
		});
		$('#telephone').keyup(function(){
				$(this).val($(this).val().replace('+620','+62'));
				if ($(this).val().length <= 3 ) $(this).val('+62');
		});

});

$(document).ready(function(){
	setTimeout(function(){
		$('.loader').fadeOut(1000);
	},1000);
});
