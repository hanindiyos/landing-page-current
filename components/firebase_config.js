var Firebase = require("firebase");
var Promise = require('bluebird');

module.exports = function(config) {
    var rootRef = new Firebase(config('firebase.basepath'));
    rootRef.authWithCustomToken(config('firebase.secret'), function(err, authData) {
        if (err)
            console.error("Cannot authenticate firebase connection");
    });
    
    var conf = config.create({});
    
    rootRef.on("value", function(snapshot) {
        conf.set(snapshot.val());
        conf.resolve();
    });
    
    return conf;
};