var path = require('path');

module.exports = function (config) {		

    return {
    	
    	getTextConfig: function(lang){
    		var text = require(path.join(__dirname,'../text/', config("main.textFile."+lang)));
    		return text;
    	}

    };
    
};
