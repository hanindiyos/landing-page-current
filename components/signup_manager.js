var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = function (config, activationService, prefineryService, telerivetService) {		

    return {
    	
    	createUser: async(function(data){
            try {
            	var firstNumber = this.checkFirstNumberPhone(data.phone);
            	var digitNumber = this.checkDigitNumberPhone(data.phone);
							var checkNumber = await(this.checkNumber(data.phone));
            	var checkEmail = await(this.checkEmail(data.email));
            	if (firstNumber == 'invalid') {
            		return {
			            'error':{
			                'message': 'After 62 the first number should be 8',
			                'type': 'telephone'
			            }
			        	}
            	}
            	if (digitNumber == 'invalid') {
            		return {
			            'error':{
			                'message': 'The minimum digit of the number is 11',
			                'type': 'telephone'
			            }
			        	}
            	}
							if (checkNumber !== null) {
            		return {
			            'error':{
			                'message': 'Phone number is already registered',
			                'type': 'telephone'
			            }
			        	}
            	}
            	if (checkEmail !== null) {
            		return {
			            'error':{
			                'message': 'Email is already registered',
			                'type': 'email'
			            }
			        	}
            	} else {
	                var tester =  await(activationService.v2ActivateTester(data));
									console.log('tester var');
									console.log(tester);
	                return tester;            		
            	}

            } catch(e) {
                return e.toString();
            }
    	}),

    	checkFirstNumberPhone: function(data){
    		if (data.indexOf('+628') == -1) {
    			return 'invalid';
    		}
    		return 'valid';
    	},

			checkDigitNumberPhone: function(data){
				if (data.replace('+','').length < 11) {
					return 'invalid';
				}
				return 'valid';
			},
			
			checkNumber: async(function(data){
				var tester = await(telerivetService.getCustomerByPhone(data));
				return tester;
			}),

			checkEmail: async(function(data){
				var tester = await(prefineryService.getTesterByEmail(data));
				return tester;
			}),

			checkID: async(function(data){
				var tester = await(prefineryService.getTesterByPrefineryId(data));
				return tester;
			})

    };
    
};
