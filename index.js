var merapi = require('merapi');
var packageJson = require('./package.json');
var configJson = require('./config.json');
var configDev = require('./config.dev.json');
var configTest = require('./config.test.json');
var configProd = require('./config.prod.json');

configJson.package = packageJson;

var container = merapi({
    basepath: __dirname,
    config: configJson,
    envConfig: {
        development: configDev,
        production: configProd,
        test: configTest
    }
});

var service = require('./service');
container.setServiceDescriptor(service);

module.exports = container;