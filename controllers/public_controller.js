var express = require('express');
var path = require('path');
process.env.TZ = 'Asia/Jakarta';

module.exports = function(app, basepath) {

	app.set('view engine', 'ejs');

    return {
    	"GET *": express.static(path.resolve(basepath,'./public'), { maxAge: 604800000 })
    };

};