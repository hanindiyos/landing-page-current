var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = function(app, config, pageManager, signupManager, textConfig) {

	return {

        "GET /": async(function(req, res) {
            var page = 'index';
            var data = {};

            var tester_id = req.cookies("idpref") || null;
// 						console.log('cookie value is '+req.cookies("idpref"));
            if ( tester_id == null ) tester_id = "0";
						if ( typeof req.cookies("idpref") === 'undefined' ) tester_id = "0";
						if ( req.cookies("idpref") == 'undefined' ) tester_id = "0";
// 						console.log("tester id: "+tester_id);
            var tester = await(signupManager.checkID(tester_id));
						if (tester !== null) page = 'index2';

            data.path = "";
	    data.refID =  req.query.ref,
            data.text = textConfig;
            data.lang = "id";
            data.data = tester;
						data.page = page;

            res.render(page, data);
        }),

        "GET /en": async(function(req, res) {
            var page = 'index';
            var data = {};

            var tester_id = req.cookies("idpref") || null;
// 						console.log('cookie value is '+req.cookies("idpref"));
            if ( tester_id == null ) tester_id = "0";
						if ( typeof req.cookies("idpref") === 'undefined' ) tester_id = "0";
						if ( req.cookies("idpref") == 'undefined' ) tester_id = "0";
// 						console.log("tester id: "+tester_id);
            var tester = await(signupManager.checkID(tester_id));
            if (tester !== null) page = 'index2';

            data.path = "";
	    data.refID =  req.query.ref,
            data.text = textConfig;
            data.lang = "en";
            data.data = tester;
						data.page = page;

            res.render(page, data);
        }),
 
		"GET /:page": async(function(req, res) {
            var page = 'index';
            var data = {};
            if(req.params.page) page = req.params.page;

            data.path = page;
            data.text = textConfig;
            data.lang = "id";
						data.page = page;

            res.render(page, data);
        }),

        "GET /en/:page": async(function(req, res) {
            var page = 'index';
            var data = {};
            if(req.params.page) page = req.params.page;

            data.path = page;
            data.text = textConfig;
            data.lang = "en";
						data.page = page;

            res.render(page, data);
        })

	}

}
