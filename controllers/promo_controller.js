var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = function(app, config, pageManager, signupManager, textConfig) {

	return {
		
				"GET /bossflight": async(function(req, res, next) {
            res.redirect('https://v1.yesbossnow.com/bossflight');
						response.end();
				}),

        "GET /:promo": async(function(req, res, next) {
            var page = req.params.promo;
						var options = textConfig("promos."+page+".options");
            var index = 'promo/index';
            var promos = textConfig("promos");
            var data = {};
						if (!promos[page])
                return next();
            
            var tester_id = req.cookies("idpref");
            if ( tester_id == null ) tester_id = "0";
						if ( typeof req.cookies("idpref") === 'undefined' ) tester_id = "0";
						if ( req.cookies("idpref") == 'undefined' ) tester_id = "0";
            var tester = await(signupManager.checkID(tester_id));
            if (tester !== null) index = 'promo/index2';
						if (options.disable_promo == "yes") index = 'promo/index3';                       
                
            data.path = page;
	    			data.refID =  req.query.ref,
            data.text = textConfig;
            data.lang = "id";
            data.data = tester;
            
            res.render(index, data);
        }),
        
        "POST /:promo/submit": async(function(req, res, next) {
            try {
                var data = {
                    'referrer_id': req.body.refid,
                    'email': req.body.email,
                    'status': 'active',
                    'invitation_code': req.body.invitation_code,                    
                    'phone': req.body.telephone
                };
                var user = await(signupManager.createUser(data));
                res.json(user);
            } catch(e) {
                res.status(500).end(e.toString());
            }
        }),
        
        "GET /en/:promo": async(function(req, res, next) {
            var page = req.params.promo;
						var options = textConfig("promos."+page+".options");
						var index = 'promo/index';
            var promos = textConfig("promos");
            var data = {};
						if (!promos[page])
                return next();
            
            var tester_id = req.cookies("idpref");
            if ( tester_id == null ) tester_id = "0";
						if ( typeof req.cookies("idpref") === 'undefined' ) tester_id = "0";
						if ( req.cookies("idpref") == 'undefined' ) tester_id = "0";
            var tester = await(signupManager.checkID(tester_id));
            if (tester !== null) index = 'promo/index2';
						if (options.disable_promo == "yes") index = 'promo/index3';
                
            data.path = page;
	    			data.refID =  req.query.ref,
            data.text = textConfig;
            data.lang = "en";
            data.data = tester;
            
            res.render(index, data);
        })

	}

}
