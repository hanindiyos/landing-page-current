var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = function(app, config, signupManager) {

	return {

		"POST /": async(function(req, res) {
            try {
                // var data = {
                //     'referrer_id': req.body.refid,
                //     'email': req.body.email,
                //     'profile': {
                //         'ip': req.headers['x-real-ip'],
                //         'telephone': req.body.telephone
                //     }
                // };
                var data = {
                    'email': req.body.email,
		    						'referrer_id': req.body.refid,
                    'phone': req.body.telephone
                };
                console.log(data);
                var user = await(signupManager.createUser(data));
								console.log(user);
                res.json(user);
            } catch(e) {
                res.status(500).end(e.toString());
            }
        })

	}

}
